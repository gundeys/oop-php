<?php

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$hewan = new animal("shaun");

echo "Name : " . $hewan ->type . "<br>";
echo "legs : " . $hewan ->legs . "<br>";
echo "cold blooded : " . $hewan ->coldBlooded . "<br><br>";

$kodok = new frog ("buduk");

echo "Name : " . $kodok -> type . "<br>";
echo "legs : " . $kodok -> legs . "<br>";
echo "cold blooded : " . $kodok -> coldBlooded . "<br>";
echo "Jump : " . $kodok -> jump() . "<br><br>";

$kera = new ape ("kera sakti");

echo "Name : " . $kera -> type . "<br>";
echo "legs : " . $kera -> legs . "<br>";
echo "cold blooded : " . $kera -> coldBlooded . "<br>";
echo "Yell : " . $kera -> yell();


?>